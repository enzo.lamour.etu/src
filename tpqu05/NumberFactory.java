package tpqu05;

public class NumberFactory {
    private NumberFactory(){}

    public static Number number(int nb){
        return Integer.valueOf(nb);
    }

    public static Number number(long nb){
        return Long.valueOf(nb);
    }

    public static Number number(float nb){
        return Float.valueOf(nb);
    }

    public static Number number(double nb){
        return Double.valueOf(nb);
    }

    public static Number number(int nb1, int nb2){
        return new Fraction(nb1,nb2);
    }
}
