package tpqu05;

public class Fraction extends Number{

    private int num;
    private int denom;

    Fraction(int num,int denom){
        this.num = num;
        this.denom = denom;
    }

    public String toString(){
        return this.num +"/"+ this.denom;
    }

    public double doubleValue(){
        return num / (double) denom;
    }

    public float floatValue(){
        return num / (float) denom;
    }

    public int intValue(){
        return num / denom;
    }

    public long longValue(){
        return num / (long) denom;
    }
    
}