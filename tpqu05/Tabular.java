package tpqu05;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Tabular {
    List<Number> tab;

    public Tabular(int size){
        tab = new ArrayList<>(Collections.nCopies(size, NumberFactory.number(0)));
    }

    public void set(int i, Number number){
        this.tab.set(i,number);
    }

    public Number max(){
        return Collections.max(tab, (n1, n2) -> ((int) (n1.floatValue() - n2.floatValue())));
    }

    @Override
    public String toString(){
        return "Tabular : "+tab;
    }

}
