package tpqu05;

public class UseTabular {
    public static void main(String [] args){
    Number n1 = NumberFactory.number(5);
    Number n2 = NumberFactory.number(55, 22);
    Number n3 = NumberFactory.number(22.5);
    Number n4 = NumberFactory.number(4);
    Number n5 = NumberFactory.number(1954);

    Tabular t1 = new Tabular(5);
    t1.set(0,n1);
    t1.set(1,n2);
    t1.set(2,n3);
    t1.set(3,n4);
    t1.set(4,n5);
    System.out.println(t1);
    System.out.println(t1.max());
    }
}
