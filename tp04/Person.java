package tp04;

public class Person {
    final private String id;
    private String name;
    private String forename;

    Person(String id, String name, String forename){
        this.id = id;
        this.name = name;
        this.forename = forename;
    }

    public String getName(){
        return this.name;
    }

    public String getId(){
        return this.id;
    }

    public String getForename(){
        return this.forename;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setId(String id){
        this.id = id;
    }

    public void setForename(String forename){
        this.forename = forename;
    }

    public boolean equals(Person p){
        return p.id == this.id;
    }

    public String toString(){
        return this.forename + this.name + this.id;
    }
}
