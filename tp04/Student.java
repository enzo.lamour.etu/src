package tp04;

public class Student {
    private double [] grades;
    private Person p;

    private Student(Person pers, double [] grades){
        this.p = pers;
        this.grades = grades;
    }

    Student(String id, String name, String forename, double [] grades){
        this(new Person(id,name,forename),grades);
    }

    public String getName(){
        return this.p.getName();
    }

    public String getForename(){
        return this.p.getForename();
    }

    public String getId(){
        return this.p.getId();
    }

    public void setName(String name){
        this.p.setName(name);;
    }

    public void setForename(String forename){
        this.p.setForename(forename);
    }

    public void setId(String id){
        this.p.setId(id);
    }

    public String toString(){
        return getName() + getForename()+ getId();
    }

    public boolean equals(Student other){
        return other.getName() == p.getName() && other.getForename() == p.getForename() && other.getId() == p.getId(); 
    }

    public double getAverage(){
        double moyenne = 0;
        for(int i = 0; i < grades.length; i ++){
            moyenne = this.grades[i];
        }
        return moyenne/grades.length;
    }

    public void addGrade(double aGrade){
        double newGrade [] = new double [this.grades.length+1];
        for(int i = 0; i < this.grades.length;i++){
             newGrade[i] = this.grades[i];
        }
        newGrade [newGrade.length] = aGrade;
        this.grades = new double [newGrade.length];
        for(int i = 0; i < this.grades.length;i++){
            this.grades[i] = newGrade[i];
       }
    }

    

}

