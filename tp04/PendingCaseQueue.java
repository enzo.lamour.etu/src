package tp04;

import tpOO.tp04.PendingCase;

public class PendingCaseQueue {
    private PendingCase [] myQueue;
    private int capacityMax;
    private int head;
    private int tail;

    PendingCaseQueue(int capacityMax){
        this.capacityMax = capacityMax;
    }

    public void clear(){
        for(int i = 0; i < capacityMax; i++){
            this.myQueue [i] = null;
        }
    }

    public PendingCase getPendingCase(int pos){
        return this.myQueue [pos];
    }

    private boolean isFullOrEmpty(){
        return head == tail;
    }

    public boolean isFull(){
        return isFullOrEmpty() && this.myQueue != null;
    }

    public boolean isEmpty(){
        return isFullOrEmpty() && this.myQueue == null;
    }

    public int indexIncrement(int num){
        num ++;
        if(this.tail >= this.capacityMax) num = 0;
        return num;
    }

    public boolean addOne(PendingCase anothingPendingCase){
        if (isFull()) return false;
        this.myQueue [this.tail] = anothingPendingCase;
        indexIncrement(this.tail);
        return true;
    }

    public int size(){
        return tail-head;
    }

    public PendingCase removeOne(){
        if(isEmpty()) return null;
        PendingCase pc = this.myQueue[this.head];
        this.myQueue[head] = null;
        head =indexIncrement(head);
        return pc;  
    }
}
