import tp04.Exo4;

public class Exo7 {
    public static int[] tab = {17, 12, 15, 38, 29, 157, 89, -22, 0, 5};
    public static int division(int index, int divisor) {
        return tab[index]/divisor;
    }
    public static void statement() {
        int x, y;
        x = LocalKeyboard.readInt("Write the index of the integer to divide: ");
        y = LocalKeyboard.readInt("Write the divisor: ");
        System.out.println("The result is: " + division(x,y));
    }


    public static void main(String[] args) throws Exception{
        int k;
        try {
        k = 1/Integer.parseInt(args[0]);
        }
        catch(ArrayIndexOutOfBoundsException e) {System.err.println("Index " + e);}
        catch(ArithmeticException e) {System.err.println("Arithmetic " + "");}
        catch(RuntimeException e) {System.err.println("Runtime " + e);}
    }

}
