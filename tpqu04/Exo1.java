public class Exo1 {
    public void example1() throws Exception{
        doIt();
    }
    
     
    public int doIt() throws Exception {
        throw new Exception();
    }

    /*catch(Exception erreur){
        System.err.println("Erreur trouvé");
    }*/

    public static void main(String [] args) throws Exception{
        (new Exo1()).example1();
    }
}

/* Il ne compile pas car exemple1 n'a pas de throws Exception ou manque un catch erreur*/