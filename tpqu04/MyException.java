public class MyException extends Exception{

    public MyException() {}

    public MyException(String msg) throws Exception {super(msg);}

    public void example2 () throws Exception{
        throw new MyException();
    }

    public static void main(String[] args) throws Exception{
       (new MyException()).example2();
    }
}
