import java.util.*;

public class Dice {
    int numberSides;
    int value;

    Dice(int numberSides, int value){
        this.numberSides = numberSides;
        this.value = value;
    }

    boolean estPositif(int numberSides){
        return numberSides > 0;
    }

    public String toString(){
        return ""+this.value;
    }
}
