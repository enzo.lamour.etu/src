import java.util.*;

public class OneDicePlayerGame {
    private Dice d;
    private DicePlayer dP;

    boolean isEnd(int totalValue){
        return totalValue == 20;
    }

    public static void main(String [] args ){
        Dice d = new Dice(6,0);
        DicePlayer dP = new DicePlayer("Alice");
        OneDicePlayerGame oDP = new OneDicePlayerGame();
        dP.play(d);
        while(!oDP.isEnd(dP.totalValue)){
            dP.play(d);
        }
        System.out.println(dP.toString());
    }
}
