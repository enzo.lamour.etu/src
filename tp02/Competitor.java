import java.util.*;

public class Competitor {
    public static final int NUMBERCOMPET = 5;
    String numberSign;
    int score;
    int time;

    Competitor(int numberSign, int score, int min, int sec){
        this.numberSign = "No"+numberSign;
        this.score = score;
        this.time = (min*60) + sec;
        if(numberSign >= 100 || numberSign < 0 || score >= 50 || score <= 0){
            this.numberSign = null;
        }
    }

    public static String display(Competitor c){
        if(c.numberSign == null){
            c.numberSign = "<INVALIDE>";
        }
        return "["+c.numberSign + " , " + c.score + " points , " + c.time + " s"+"]";
    }

    public static void main (String [] args){
        Competitor c;
        int [] tab = new int []{1,45,15,20,
                                2,32,12,45,
                                5,12,13,59,
                                12,12,15,70,
                                32,75,15,20};
        for(int i = 0; i < NUMBERCOMPET;i ++){
            c = new Competitor(tab[i], tab[i+1], tab[i+2], tab[i+3]);
            System.out.println(display(c));
        }
    }


}
