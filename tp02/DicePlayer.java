import java.util.*;

public class DicePlayer {
    int totalValue;
    int nbDiceRolls;
    String name;
    public static final Random RAND = new Random();

    DicePlayer(String name){
        this.name = name;
        this.nbDiceRolls = 0;
        this.totalValue =0;
    }

    void play(Dice dice6){
        dice6.value = roll(dice6.numberSides);
        this.totalValue += dice6.value;
        this.nbDiceRolls ++;
    }

    public static int roll(int numberSides){
        return RAND.nextInt(numberSides);
    }

    public String toString(){
        return this.name + ": "+this.totalValue+" points en "+this.nbDiceRolls+" coups";
    }
}
