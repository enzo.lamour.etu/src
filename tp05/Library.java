package tp05;

import java.util.ArrayList;
import java.lang.StringBuilder;

public class Library {
    private ArrayList<Book> catalog;

    public Library() {
	this.catalog = new ArrayList<Book>();
    }
    
    public Book getBook(String s) {
	for (Book b : catalog) {
	    if (b.getCode().equals(s)) {
		return b;
	    }
	}
	return null;
    }

    public boolean addBook(Book b) {
	if (catalog.contains(b)) return false;
	return catalog.add(b);
    }

    public boolean removeBook(Book b) {
	if (!catalog.contains(b)) return false;
	return catalog.remove(b);
    }

    public boolean removeBook(String s) {
	if (!catalog.contains(getBook(s))) return false;
	return catalog.remove(this.getBook(s));
    }

    public String borrowings() {
	StringBuilder sb = new StringBuilder();
	sb.append("Borrowings{\n");
	for (Book b : catalog) {
	    if (!b.isAvailable()) {
		sb.append(b);
		sb.append('\t');
		sb.append("backDate:"+b.getGiveBackDate());
		sb.append(",\n");
	    }
	}
	sb.append('}');
	return sb.toString();
    }

    public boolean borrow(String code, int borrower) {
	return this.getBook(code).borrow(borrower);
    }

    public boolean giveBack(String code) {
	return this.getBook(code).giveBack();
    }
    
    public String toString() {
	StringBuilder sb = new StringBuilder();
	sb.append("Library{");
	for (Book b : catalog) {
	    sb.append(b);
	    sb.append(",\n");
	}
	sb.append('}');
	return sb.toString();
    }

    
}
