package tp05.shop;
import java.util.ArrayList;
public class Shop {
    private ArrayList<Article> catalog = new ArrayList<Article>();

    public String toString() {
	StringBuilder sb = new StringBuilder();
	for (Article a : catalog) {
	    sb.append(a);
	    sb.append('\n');
	}
	sb.deleteCharAt(sb.length() - 1);
	return sb.toString();
    }

    public void addArticle(Article a) {
	catalog.add(a);
    }

    public void addArticle(ArrayList<Article> aa) {
	for (Article a : aa) {
	    catalog.add(a);
	}
    }

    public List<PerishableArticle> getPerishables() {
	ArrayList<Article> aa = new ArrayList<>();

	for (Article a : catalog) {
	    if (a.isPerishable()) {
		aa.add(a);
	    }
	}
	return aa;
    }

    public int getNbArticle() {
	return catalog.size();
    }

    public int getNbPerishable() {
	return getPerishables().size();
    }
}
