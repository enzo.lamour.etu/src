package tp05.shop;

public class Article {
    private final String idRef, label;
    private final double PURCHASE_PRICE;
    private double salePrice;

    private static final double DEFAULT_MARGIN = 0.1;
    
    public Article(String idRef, String label, double purchasePrice, double salePrice) {
	this.idRef = idRef;
	this.label = label;
	this.PURCHASE_PRICE = purchasePrice;
	this.salePrice = salePrice;
    }

    public Article(String idRef, String label, double purchasePrice) {
	this(idRef, label, purchasePrice, purchasePrice * (DEFAULT_MARGIN+1) );
    }

    public String toString() {
	return "Article ["+idRef+":"+label+"="+PURCHASE_PRICE+"e/"+getMargin()+"e]";
    }
    
    public double getSalePrice() {
	return salePrice;
    }

    public void setSalePrice(double salePrice) {
	this.salePrice = salePrice;
    }

    public double getPurchasePrice() {
	return PURCHASE_PRICE;
    }

    public double getMargin() {
	return salePrice - PURCHASE_PRICE;
    }

    public boolean isPerishable() {
	return false;
    }
}
