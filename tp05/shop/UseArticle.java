package tp05.shop;
import java.util.ArrayList;
public class UseArticle {
    public static void scenario1() {
	Article a1 = new Article("a1", "comment manger", 10);
	Article a2 = new Article("a2", "comment boire", 10);
	Article a3 = new Article("a3", "comment marcher", 10);
	Article a4 = new PerishableArticle("a4", "comment périr", 10);
	Article a5 = new PerishableArticle("a5", "comment dépérir", 10);

	ArrayList<Article> aa = new ArrayList<>();
	aa.add(a1);
	aa.add(a2);
	aa.add(a3);
	aa.add(a4);
	aa.add(a5);
	
	Shop s = new Shop();
	s.addArticle(aa);

	System.out.println(s);
    }
}
