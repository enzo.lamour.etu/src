package tp05.shop;

import java.time.LocalDate;
import java.util.ArrayList;

public class PerishableArticle extends Article {
    private LocalDate deadline;

    private static final int DEFAULT_DEADLINE = 10;
    
    public PerishableArticle(String idRef, String label, double purchasePrice, double salePrice, LocalDate deadline) {
	super(idRef, label, purchasePrice, salePrice);
	this.deadline = deadline;
    }

    public PerishableArticle(String idRef, String label, double purchasePrice, LocalDate deadline) {
	super(idRef, label, purchasePrice);
	this.deadline = deadline;
    }

    public PerishableArticle(String idRef, String label, double purchasePrice, double salePrice) {
	this(idRef, label, purchasePrice, salePrice, LocalDate.now().plusDays(DEFAULT_DEADLINE));
    }
    
    public PerishableArticle(String idRef, String label, double purchasePrice) {
	this(idRef, label, purchasePrice, LocalDate.now().plusDays(DEFAULT_DEADLINE));
    }
    
    public boolean isPerishable() {
	return true;
    }

    public String toString() {
	return "PerishableArticle ["+super.toString().substring(9, super.toString().length() - 1)+"-->"+deadline+"]";
    }
}
