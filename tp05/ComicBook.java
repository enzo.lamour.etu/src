package tp05;

import java.time.LocalDate;

public class ComicBook extends Book {
    private String illustrator;

    private static final int MAX_DURATION = 5; 
    
    public ComicBook(String code, String title, String author, String illustrator, int publicationYear) {
	super(code, title, author, publicationYear);
	this.illustrator = illustrator;
    }

    public ComicBook(String code, String title, String author, int publicationYear) {
        this(code, title, author, author, publicationYear);
    }

    public String toString() {
	return "ComicBook:" + super.toString().substring(4, super.toString().length()-1) + ",illustrator:"+illustrator+"}";
    }

    public int getMaxDuration() {
	LocalDate now = LocalDate.now();
	
	if (LocalDate.of(this.publicationYear, 1, 1).plusYears(2).isBefore(LocalDate.now())) {
	    return super.getMaxDuration();
	} else {
	    return this.MAX_DURATION;
	}
    }
}
