package tp05;

public class UseLibrary {
    public static void scenario1() {
	Book b1 = new Book("H2G2", "The Hitchhiker’s Guide to the Galaxy", "D.Adams", 1979);
	Book b2 = new Book("FLTL", "Flatland", "E.Abbott Abbott", 1884);
	Book b3 = new Book("REU", "The Restaurant at the End of the Universe", "D.Adams", 1980);
	Library l = new Library();
	l.addBook(b1);
	l.addBook(b2);
	l.addBook(b3);
	System.out.println(l);
	System.out.println(l.getBook("H2G2"));
    }

    public static void scenario2() {
	Book b1 = new Book("H2G2", "The Hitchhiker’s Guide to the Galaxy", "D.Adams", 1979);
	Book b2 = new Book("FLTL", "Flatland", "E.Abbott Abbott", 1884);
	Book b3 = new Book("REU", "The Restaurant at the End of the Universe", "D.Adams", 1980);
	Library l = new Library();
	int bw1 = 42;
	int bw2 = 404;

	l.addBook(b1);
	l.addBook(b2);
	l.addBook(b3);

	System.out.println(l.borrowings());
	
	System.out.println(l);
	l.borrow("H2G2", bw1);
	System.out.println(l);
	l.borrow("H2G2", bw2);
	System.out.println(l);
	l.borrow("FLTL", bw2);
	System.out.println(l);
	l.borrow("REU", bw1);
	System.out.println(l);

	System.out.println(l.borrowings());
    }

    public static void scenario3() {
	Book b1 = new Book("H2G2", "The Hitchhiker’s Guide to the Galaxy", "D.Adams", 1979);
	Book b2 = new Book("FLTL", "Flatland", "E.Abbott Abbott", 1884);
	Book b3 = new Book("REU", "The Restaurant at the End of the Universe", "D.Adams", 1980);
	Book b4 = new ComicBook("T1T1", "Tintin Au Congo", "Hergé", 1931);
	Library l = new Library();

	l.addBook(b1);
	l.addBook(b2);
	l.addBook(b3);
	l.addBook(b4);

	System.out.println(l);
    }

    public static void scenario4() {
	Book b1 = new Book("H2G2", "The Hitchhiker’s Guide to the Galaxy", "D.Adams", 1979);
	Book b2 = new Book("FLTL", "Flatland", "E.Abbott Abbott", 1884);
	Book b3 = new Book("REU", "The Restaurant at the End of the Universe", "D.Adams", 1980);
	Book b4 = new ComicBook("T1T1", "Tintin Au Congo", "Hergé", 1931);
	Book b5 = new ComicBook("BLOT", "Blanche Lourde Otarie Tachetée", "Maxime", 2022);
	Library l = new Library();

	l.addBook(b1);
	l.addBook(b2);
	l.addBook(b3);
	l.addBook(b4);
	l.addBook(b5);

	l.borrow("T1T1", 0);
	l.borrow("BLOT", 0);
	l.borrow("H2G2", 0);
	
	System.out.println(l.borrowings());
	
    }
}
