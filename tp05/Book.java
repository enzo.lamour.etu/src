package tp05;

import java.time.LocalDate;

public class Book {
    private String code;
    private int borrower;
    private boolean borrowed;
    private LocalDate borrowingDate;
    
    private final String title, author;
    protected final int publicationYear;
    private static final int MAX_DURATION = 15;
    
    public Book(String code, String title, String author, int publicationYear) {
	this.code = code;
	this.title = title;
	this.author = author;
	this.publicationYear = publicationYear;
	this.borrowed = false;
	this.borrower = Integer.MIN_VALUE;
    }

    public boolean borrow(int borrower) {
	if (isAvailable()) {
	    this.borrowingDate = LocalDate.now();
	    this.borrowed = true;
	    this.borrower = borrower;
	    return true;
	}
	return false;
    }

    public boolean giveBack() {
	if (!isAvailable()) {
	    this.borrowed = false;
	    this.borrower = Integer.MIN_VALUE; 
	}
	return false;
    }
    
    public boolean isAvailable() {
	return !borrowed;
    }
    
    public String toString() {
	return "Book{code:"+code+",title:"+title+",author:"+author+",year:"+publicationYear+",borrower:"+borrower+"}";
    }

    public int getMaxDuration() {
	return MAX_DURATION;
    }

    public LocalDate getGiveBackDate() {
	return borrowingDate.plusDays(getMaxDuration());
    }

    public LocalDate getBorrowingDate() {
	return borrowingDate;
    }

    public String getCode() {
	return this.code;
    }

}
