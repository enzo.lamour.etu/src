package exo2;
import java.time.LocalDate;
import java.util.Scanner;

public class UseLocalDate {

    static String dateOfTheDay(){
        return "Today's date is "+ java.time.LocalDate.now();
    }

    static LocalDate inputDate(){
        final Scanner keyboard = new Scanner(System.in);
        final int year = keyboard.nextInt();
        final int month = keyboard.nextInt();
        final int day = keyboard.nextInt();
        return LocalDate.parse(""+year+month+day);
    }

    /*static String diffDate(){

    }*/

    public static void main(String [] args){
        System.out.println(dateOfTheDay());
        System.out.println(inputDate());
    }
}
