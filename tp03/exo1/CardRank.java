/**
*
*
* @author Lamour Enzo
*/

package exo1;
public enum CardRank {
    SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE;
}

/* Un enum de tout les rank des cartes */
