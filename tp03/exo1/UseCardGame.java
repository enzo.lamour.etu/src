/**
*
*
* @author Lamour Enzo
*/

package exo1;
public class UseCardGame {
    public static void main(String[] args) {
        final CardGame c1 = new CardGame(CardColor.HEART, CardRank.TEN);
        final CardGame c2 = new CardGame(CardColor.HEART, CardRank.JACK);
        final CardGame c3 = new CardGame(CardColor.DIAMOND, CardRank.TEN);
        final CardGame c4 = new CardGame(CardColor.CLUB, CardRank.SEVEN);
        final CardGame c5 = new CardGame(CardColor.SPADE, null);
        final CardGame c6 = new CardGame(null, CardRank.JACK);
        final CardGame c7 = new CardGame(CardColor.HEART, CardRank.TEN);
        // equals scenario
        if(!c1.equals(c1) || c1.equals(null) || c1.equals(c2) || c1.equals(c3) || c1.equals(c4) || c1.equals(c5) || c1.equals(c6) || !c1.equals(c7)){
            System.out.println("equals FAILED");
        }
        // compareColor scenario
        else if(c1.compareColor(c1) != 0 || c1.compareColor(c2) != 0 || c1.compareColor(c3) <= 0 || c1.compareColor(c4) <= 0 || c1.compareColor(c5) >= 0 || c1.compareColor(c7) != 0){
            System.out.println("compareColor FAILED");
        }
        // compareRank scenario
        else if(c1.compareRank(c1) != 0 || c1.compareRank(c2) >= 0 || c1.compareRank(c3) != 0 || c1.compareRank(c4) <= 0 || c1.compareRank(c6) >= 0 || c1.compareRank(c7) != 0){
            System.out.println("compareRank FAILED");
        }else{
            System.out.println("Tests OK");
        } 
    }

    /* Class qui permet de verifier si les autres classes fonctionne */

}


