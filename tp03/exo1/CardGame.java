/**
*
*
* @author Lamour Enzo
*/

package exo1;

public class CardGame {
    private static CardColor color;
    private static CardRank rank;
    String color2;
    String rank2;

    public CardGame(CardColor color ,CardRank rank){
        this.color = color;
        this.rank = rank;
    }

    public CardGame(String color, String  rank){
        color2 = color;
        rank2 = rank;
    }

    public CardColor getColor(){
        return this.color;
    }

    public CardRank getRank(){
        return this.rank;
    }

    public int compareRank(CardGame CardGame){
        return CardGame.rank.ordinal() - CardGame.rank.ordinal();
    }

    public int compareColor(CardGame CardGame){
        return CardGame.color.ordinal() - CardGame.color.ordinal();
    }

    public boolean isBefore(CardGame CardGame){
        return compareRank(CardGame) < 0 && compareColor(CardGame) < 0;
    }

    public boolean equals(CardGame CardGame){
        return this.compareRank(CardGame) == 0 && this.compareColor(CardGame) == 0;
    }

    /*public String toString(){

    }*/
}


