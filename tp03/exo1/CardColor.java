/**
*
*
* @author Lamour Enzo
*/

package exo1;
public enum CardColor {
    CLUB, DIAMOND, HEART, SPADE;
}

/* Un Enum des couleurs de toute les cartes */
