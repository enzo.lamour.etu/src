import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class DicoJava {
    
    public static void main(String [] args) throws FileNotFoundException, IOException{
        FileReader f = new FileReader("./DicoJava.txt");
        try(BufferedReader bf = new BufferedReader(f)){
            int c = f.read();
            while(c!=-1){
                System.out.println(f.readLine());
                c = f.read();
            }
        }
        catch(FileNotFoundException ie){
            System.err.println("Fichier pas toruvé" + ie.getMessage());
        }
        catch(IOException ie){
            System.err.println("IO :" + ie.getMessage());
        }

    }
}
