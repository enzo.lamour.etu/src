import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

class LoginManagement {

    public final String LOGIN;
    public final String PWD;

    LoginManagement(String login, String pwd){
        this.LOGIN = login;
        this.PWD = pwd;
    }

    public String getPwd(){
        return this.PWD;
    }

    public String getLog(){
        return this.LOGIN;
    }

    public boolean getUserPwd(String mdp) throws Exception{
        return this.getPwd().equals(mdp);
    }

    public boolean getUserLog(String log) throws Exception{
        return this.getLog().equals(log);
    }

    public static void main(String [] args) throws Exception{
        LoginManagement log = new LoginManagement("Alfred", "alfred62");
        InputStreamReader usr = new InputStreamReader(System.in);
        try(BufferedReader br =  new BufferedReader(usr)){
            String apLog = " ";
            String apPwd = " ";
            while(!log.getUserLog(apLog)){
                System.out.print("Quelle est ton login : \n");
                apLog = br.readLine();  
            }
            while(!log.getUserPwd(apPwd)){
                System.out.println("Quelle est votre mdp : ");
                apPwd = br.readLine();       
            }
        }
        catch(IOException wrongPwdException){
            System.err.println("Mauvais password");
        }
    }
}