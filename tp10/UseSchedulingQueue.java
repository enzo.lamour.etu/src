package tp10;
import tp05.*;
import tp10.IScheduler;
import tp10.SchedulingQueue;

public class UseSchedulingQueue {

    public void main (String []strs ){
        Book b1 = new Book("OPM","One punch Man","Yusuke Murata","2019");
        Book b2 = new Book("BCH","Batman la cour des hiboux","Scott Snyder","2019");
        Book b3 = new Book("BKJ","Batman Killing Joke","Alan Moore","1988");

        Task t1 = new Task("Manager",3);
        Task t2 = new Task("Boire",2);
        Task t3 = new Task("Pouette",3);

        IScheduler<Book> library = new SchedulingQueue<>();
        IScheduler<Task> toDoList = new SchedulingQueue<>();

        library.addElement(b1);
        library.addElement(b2);
        toDoList.addElement(t1);

        System.out.println(library + "\n"+toDoList);
    }
}
