package tp10;

public interface IScheduler<T> {
    
    public void addElement(T t);

    public T highestPriority();
    
    public boolean isEmpty();

    public int size();
}
