package tp10;

public class Task implements IPriority{
    public int priority;
    public String label;

    Task(String labal, int priority){
        this.priority = priority;
        this.label = labal;
    }

    public int getPriority(){
        return this.priority;
    }

    public String getLabel(){
        return this.label;
    }

    public void setLabel(){
        this.label = label;
    }

    public String toString(){
        return getClass().getSimpleName()+" : "+this.label + " : " + this.priority;
    }
}
