package tp10;

import java.util.LinkedList;

public class SchedulingQueue<T> implements IScheduler<T> {
    protected LinkedList<T> theQueue;
    static int idcount = 0;
    final private int id = 0;


    public SchedulingQueue(){
        this.theQueue = new LinkedList<>();
        this.id = idcount++;
    }

    public int getID(){
        return this.ID;
    }

    @Override
    public void addElement(T element){
        this.theQueue.add(element);
    }

    @Override
    public T highestPriority(){
        return this.theQueue.poll();
    }

    @Override
    public boolean isEmpty(){
        return this.theQueue.isEmpty();
    }

    @Override
    public int size(){
        return this.theQueue.size();
    }

    @Override
    public String toString(){
        return "Queue"+this.id + " -> " +this.theQueue;
    }
}
