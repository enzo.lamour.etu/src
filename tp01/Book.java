
public class Book {
    // class attributes
    String author;
    String title;
    String year;
    // constructor
    Book(String author, String title, String year) {
        this.author = author;
        this.title = title;
        this.year = year;
    }
    // methods
    String getAuthor() {
        return this.author;
    }
    String getTitle() {
        return this.title;
    }

    String getYear() {
        return this.year;
    }

    public static String [] useBook(){
        String [] biblio = new String []{"Dimitri Gloukhovski","Métro 2034","2002",
                                "Kentarō Miura","Berserk","1989",
                                "Andrzej Sapkowski","The Witcher","1986"
                               };
        return biblio;
    }

    public static void initialiseBook(String [] biblio){
        for(int i = 0; i < biblio.length; i ++){
            if(i == 0 || i ==3 || i == 6){
                System.out.print(biblio[i]+" a ecrit ");

            }else if(i == 1 || i ==4 || i ==7){
                System.out.print(biblio[i]+" en ");
            }else{
                System.out.print(biblio[i]);
            }
            if(i == 2 || i == 5 || i == 8){
                System.out.println();
            }
        }
    }

    public static void main(String [] args){
        initialiseBook(useBook());
    }
}
