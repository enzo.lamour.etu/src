import java.util.*;

public class Irregular {
    public static final int LINESIZE = 3;
    public static final Random RAND = new Random();
    int [] l;

    Irregular(int [] lineSize){
        this.l = new int [lineSize.length];
    }

    public static int [] randomFilling(int [] tab){
        for(int i = 0; i < tab.length; i++){
            if(RAND.nextInt(LINESIZE) <= RAND.nextInt(4)){
                 tab[i] = RAND.nextInt(9);
            } 
        }
        return tab;
    }

    public static String display(){
        return " | ";
    }
    public static void main(String [] arg){
        int [] plateau = new int [LINESIZE];
        Irregular r = new Irregular(plateau);
        for(int i = 0; i < LINESIZE; i++ ){
            System.out.print(display());
            for(int j = 0; j < 3; j++){
                randomFilling(r.l);
                if(r.l[i] == 0){
                    System.out.print(" ");
                }else{
                    System.out.print(r.l[i]);  
                }
                System.out.print(display());
            }
            System.out.println();
        }
    }
}

