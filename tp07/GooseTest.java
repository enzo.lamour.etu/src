package tpOO.tp07;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class GooseTest {
    public int id1, id2, id3;
    public double w1, w2, w3;
    public Goose g1, g2, g3;

    @BeforeEach
    void testInitialization() {
        id1=10; id2=20; id3=30;
        w1=1.0; w2=2.0; w3=3.0;
        g1 = new Goose(id1, w1);
        g2 = new Goose(id2, w2);
        g3 = new Goose(id3, w3);
    }

    @Test
    void testGetIdentity() {
        assertEquals(id1, g1.getIdentity());
        assertEquals(id2, g2.getIdentity());
        assertEquals(id3, g3.getIdentity());
    }

    @Test
    void testSetIdentity() {
        assertEquals(id1, g1.getIdentity());
        g1.setIdentity(id2);
        assertEquals(id2, g1.getIdentity());
    }

    @Test
    void testGetWeight() {
        assertEquals(w1, g1.getWeight());
        assertEquals(w2, g2.getWeight());
        assertEquals(w3, g3.getWeight());
    }

    @Test
    void testSetWeight() {
        assertEquals(w1, g1.getWeight());
        g1.setWeight(w2);
        assertEquals(w2, g1.getWeight());
    }

    @Test
    void testGetPrice() {
        assertEquals(w1*g1.getPriceKg(), g1.getPrice());
        assertEquals(w2*g2.getPriceKg(), g2.getPrice());
        assertEquals(w3*g3.getPriceKg(), g3.getPrice());
    }
}
