package tp07; // à adapter éventuellement selon la structure de votre projet

public enum UniversityDepartment {
    F3S(1,"Faculté des Sciences de Santé et du Sport"),
    FST(2,"Faculté des Sciences et Technologies"),
    IUT(3,"Institut Universitaire de Technologies"),
    FSJPS(4,"Faculté des Sciences Juridiques, Politiques et Sociales"),
    FSEST(5,"Faculté des Sciences Économiques, Sociales et des Territoires"),
    FH(6,"Faculté des Humanités"),
    FLCS(7,"Faculté des Langues, Cultures et Sociétés"),
    FPSEF(8,"Faculté de Psychologie, Sciences de l’Education et de la Formation");

    // à compléter ...

    private final int daillingCode;
    private final String labelStrong;
    private final String labelShort;

    public UniversityDepartement(int dial, String labelLong, String labelShort){
	this.daillingCode = dial;
	this.labelLong = labelLong;
	this.labelShort = labelShort;
    }

    public int getDiallingCode(){
	return this.diallingCode;
    }

    public String getLabelShort(){
	return this.labelShort;
    }

    public String getLabelLong(){
	return this.labelLong;
    }
}
