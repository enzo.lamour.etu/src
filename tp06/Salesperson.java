package tp06;

import java.time.LocalDate;

public abstract class Salesperson extends Employe {
    private double turnover;
    private static double objective = 10000.0;

    Salesperson(String name, LocalDate hiringDate,double turnover){
        super(name, hiringDate);
        this.turnover = turnover;
    }

    public double getTurnover() {
        return turnover;
    }

    public String toString(){
        return super.toString() + ":"+turnover;
    }

    public boolean isSalesPerson(){
        return true;
    }

    public boolean isWorker(){
        return false;
    }

/*     public boolean objectiveFulfilled(){
        return 
    }*/
}
