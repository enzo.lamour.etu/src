package tp06;

import java.time.LocalDate;
import java.util.ArrayList;

public class Compagny {
    private String name;
    private ArrayList<Employe> staff;

    Compagny(){
        this.staff = new ArrayList<Employe>();
    }
    
    public void addEmploye(Employe e){
        this.staff.add(e);
    }

    public void supprEmploye(int idx){
        this.staff.remove(idx);
    }

    public void supprEmploye(Employe e){
        this.staff.remove(staff.indexOf(e));
    }

    public String toString(){
        return this.staff.toString() +":"+this.name;
    }

    public int getNbEmploye(){
        return this.staff.size();
    }

    public int getNbSalesPerson(){
        int cpt=0;
        for(Employe e : staff){
            if(e.isSalesPerson()){
                cpt++;
            }
        }
        return cpt;
    }

    public int getNbWorker(){
        int cpt = 0;
        for(Employe e : staff){
            if(e.isWorker()){
                cpt++;
            }
        }
        return cpt;
    }

    public void firing(LocalDate fatefulDate){
        ArrayList<Employe> temp = new ArrayList<>();

        for(Employe e : staff){
            if(e.getHirringDate().isAfter(fatefulDate)) temp.add(e);
        }
        staff.removeAll(temp);
    }

/*     public void firring(){
        ArrayList<Employe> temp = new ArrayList<>();

        for(Employe e : staff){
            if(e.isWorker()){
                if(e.getWages()== e.Worker.objective < e.getObjective()) temp.add(e);
            }        
        }
    }*/

}
