package tp06;

import java.time.LocalDate;

public abstract class TravellingSalesperson extends Salesperson{
    private static final double PERCENTAGE = 20/100;
    private static final int BONUS = 800;

    TravellingSalesperson(String name, LocalDate hiringDate, double turnover) {
        super(name, hiringDate, turnover);
    }

    public String getTitle(){
        return super.getClass().getName();
    }

    public double getWages(){
        return super.getTurnover()*PERCENTAGE+BONUS;
    }

}