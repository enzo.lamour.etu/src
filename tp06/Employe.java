package tp06;

import java.time.LocalDate;

public abstract class Employe {
    private String name;
    private LocalDate hiringDate;

    Employe(String name, LocalDate hiringDate){
        this.name = name;
        this.hiringDate = hiringDate;
    }

    public LocalDate getHirringDate(){
        return this.hiringDate;
    }

    public String getName(){
        return this.name;
    }
    
    public String toString(){
        return getTitle() + getName();
    }
    
    public String getTitle(){
        return this.name +" "+ this.hiringDate;
    }

    public abstract double getWages();

    public boolean isSalesPerson(){
        return false;
    }

    public boolean isWorker(){
        return false;
    }

//    public int getObjective() {
//      return super.getObjective();
//  }
}
