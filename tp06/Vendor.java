package tp06;

import java.time.LocalDate;

public abstract class Vendor extends Salesperson{
    private final double PERCENTAGE = 20/100;
    private final int BONUS = 400;

    Vendor(String name, LocalDate hiringDate, double turnover) {
        super(name, hiringDate, turnover);
    }

    public String getTitle() {
        return super.getClass().getName();
    }

    public double getWages(){
        return super.getTurnover()*PERCENTAGE + BONUS;
    }
    
}
