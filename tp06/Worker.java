package tp06;

import java.time.LocalDate;

public abstract class Worker extends Employe{
    private final static double BY_UNIT = 5;
    private static int objective = 1000;
    private int units;

    Worker(String name, LocalDate hiringDate, int units) {
        super(name, hiringDate);
        this.units = units;
    }

    public int getObjective(){
        return objective;
    }

    public String getTitle(){
        return super.getClass().getName();
    }

    public double getWages(){
        return units*BY_UNIT;
    }

    public String toString(){
        return super.toString() + ":" + units;
    }

    public boolean isSalesPerson(){
        return false;
    }

    public boolean isWorker(){
        return true;
    }
}
