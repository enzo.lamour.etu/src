package tpQU.tp03;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class DefenseTest {
    public LocalDate day1, day2;
    public LocalTime time1, time2;
    public Student s1, s2, s3;
    public Defense d1, d2, d3;
    public String titleA, titleB, titleC;

    @BeforeEach
    public void initialization() {
        titleA = "defenseA";
        titleB = "defenseB";
        titleC = "defenseC";
        day1 = LocalDate.of(2022, 2, 2);
        day2 = LocalDate.of(2023, 3, 3);
        time1 = LocalTime.of(11, 11);
        time2 = LocalTime.of(22, 22);
        s1 = new StudentBUT1("Alice", "a");
        s2 = new StudentBUT1("Bruno", "b");
        s3 = new StudentBUT2("Clément", "c");
        d1 = new Defense(Room.r0A20, s1, titleA);
        d2 = new Defense(day1, Room.r0A20, s2, time1, titleB);
        d3 = new Defense(day2, Room.r0A49, s3, time2, titleC);
    }
    
    @Test
    void testGetDay() {
        assertNull(d1.getDay());
        assertEquals(day1, d2.getDay());
        assertEquals(day2, d3.getDay());
    }

    @Test
    void testGetDuration() {
        assertEquals(s1.getDuration(), d1.getDuration());
        assertEquals(s2.getDuration(), d2.getDuration());
        assertEquals(s3.getDuration(), d3.getDuration());
    }

    @Test
    void testGetHour() {
        assertNull(d1.getHour());
        assertEquals(time1, d2.getHour());
        assertEquals(time2, d3.getHour());
    }

    @Test
    void testSetDay() {
        assertNull(d1.getDay());
        d1.setDay(day1);
        assertEquals(day1, d1.getDay());
        d1.setDay(day2);
        assertEquals(day2, d1.getDay());
        d1.setDay(LocalDate.now());
        assertEquals(LocalDate.now(), d1.getDay());
        d1.setDay(null);
        assertNull(d1.getDay());
    }

    @Test
    void testSetHour() {
        assertNull(d1.getHour());
        d1.setHour(time1);
        assertEquals(time1, d1.getHour());
        d1.setHour(time2);
        assertEquals(time2, d1.getHour());
        d1.setHour(null);
        assertNull(d1.getHour());
    }

    @Test
    void testToString() {
        String result = d1.getDay() + ":" + d1.getHour() + " in " + d1.getPlace() + " --> " + d1.getStudent() + " :: " + d1.getTitle();
        assertEquals(result, d1.toString());
        result = d2.getDay() + ":" + d2.getHour() + " in " + d2.getPlace() + " --> " + d2.getStudent() + " :: " + d2.getTitle();
        assertEquals(result, d2.toString());
        result = d3.getDay() + ":" + d3.getHour() + " in " + d3.getPlace() + " --> " + d3.getStudent() + " :: " + d3.getTitle();
        assertEquals(result, d3.toString());
    }
}
