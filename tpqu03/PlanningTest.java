package tpQU.tp03;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class PlanningTest {
    public LocalDate day1, day2;
    public LocalTime time1, time2;
    public Student s1, s2, s3;
    public Defense d1, d2, d3;
    public String titleA, titleB, titleC;
    public Planning plan;

    @BeforeEach
    public void initialization() {
        titleA = "defenseA";
        titleB = "defenseB";
        titleC = "defenseC";
        day1 = LocalDate.of(2022, 2, 2);
        day2 = LocalDate.of(2023, 3, 3);
        time1 = LocalTime.of(11, 11);
        time2 = LocalTime.of(22, 22);
        s1 = new StudentBUT1("Alice", "a");
        s2 = new StudentBUT1("Bruno", "b");
        s3 = new StudentBUT2("Clément", "c");
        d1 = new Defense(Room.r0A20, s1, titleA);
        d2 = new Defense(Room.r0A20, s2, titleB);
        d3 = new Defense(day2, Room.r0A49, s3, time1, titleC);
        plan = new Planning(day1);
    }

    @Test
    void testAddDefense() {
        assertEquals(0, plan.getNbDefense());
        plan.addDefense(d1);
        assertEquals(1, plan.getNbDefense());
        assertEquals(d1, plan.getDefense(0));
        plan.addDefense(d2);
        assertEquals(2, plan.getNbDefense());
        assertEquals(d1, plan.getDefense(0));
        assertEquals(d2, plan.getDefense(1));
    }

    @Test
    void testAddDefense2() {
        ArrayList<Defense> tmp = new ArrayList<Defense>();
        tmp.add(d1); tmp.add(d2);
        assertEquals(0, plan.getNbDefense());
        plan.addDefense(tmp);
        assertEquals(2, plan.getNbDefense());
        assertEquals(d1, plan.getDefense(0));
        assertEquals(d2, plan.getDefense(1));
    }

    @Test
    void testSetDay() {
        assertEquals(day1, plan.getDay());
        plan.setDay(LocalDate.now());
        assertEquals(LocalDate.now(), plan.getDay());
    }

    @Test
    void testGetDefense() {
        plan.addDefense(d1); plan.addDefense(d2); plan.addDefense(d3);
        assertNull(plan.getDefense(-1));
        assertEquals(d1, plan.getDefense(0));
        assertEquals(d2, plan.getDefense(1));
        assertEquals(d3, plan.getDefense(2));
        assertNull(plan.getDefense(3));
    }

    @Test
    void testScheduling() {
        assertNull(d1.getHour());
        assertNull(d1.getDay());
        assertNull(d2.getHour());
        assertNull(d2.getDay());
        assertEquals(time1, d3.getHour());
        assertEquals(day2, d3.getDay());
        assertEquals(0, plan.getNbDefense());
        plan.addDefense(d1); plan.addDefense(d2); plan.addDefense(d3);
        plan.scheduling();
        assertEquals(d1, plan.getDefense(0));
        assertEquals(d2, plan.getDefense(1));
        assertEquals(d3, plan.getDefense(2));
        assertEquals(day1, d1.getDay());
        assertEquals(day1, d2.getDay());
        assertEquals(day1, d3.getDay());
        assertEquals(Planning.defaultStart, d1.getHour());
        assertEquals(Planning.defaultStart.plusMinutes(d1.getDuration() + Planning.breakDuration), d2.getHour());
        assertEquals(Planning.defaultStart.plusMinutes(d1.getDuration() + d2.getDuration() + (Planning.breakDuration*2)), d3.getHour());
    }

    @Test
    void testScheduling2() {
        assertEquals(0, plan.getNbDefense());
        assertNull(d1.getHour());
        assertNull(d1.getDay());
        assertNull(d2.getHour());
        assertNull(d2.getDay());
        assertEquals(time1, d3.getHour());
        assertEquals(day2, d3.getDay());
        assertEquals(0, plan.getNbDefense());
        plan.addDefense(d1); plan.addDefense(d2); plan.addDefense(d3);
        plan.scheduling(time2);
        assertEquals(d1, plan.getDefense(0));
        assertEquals(d2, plan.getDefense(1));
        assertEquals(d3, plan.getDefense(2));
        assertEquals(time2, d1.getHour());
        assertEquals(time2.plusMinutes(d1.getDuration() + Planning.breakDuration), d2.getHour());
        assertEquals(time2.plusMinutes(d1.getDuration() + d2.getDuration() + (Planning.breakDuration*2)), d3.getHour());
    }

    @Test
    void testSwapDefense() {
        assertEquals(0, plan.getNbDefense());
        plan.addDefense(d1); plan.addDefense(d2); plan.addDefense(d3);
        plan.scheduling(time2);
        assertEquals(d1, plan.getDefense(0));
        assertEquals(d2, plan.getDefense(1));
        assertEquals(d3, plan.getDefense(2));
        assertEquals(time2, d1.getHour());
        assertEquals(time2.plusMinutes(d1.getDuration() + Planning.breakDuration), d2.getHour());
        assertEquals(time2.plusMinutes(d1.getDuration() + d2.getDuration() + (Planning.breakDuration*2)), d3.getHour());
        plan.swapDefense(d1, d3);
        assertEquals(d3, plan.getDefense(0));
        assertEquals(d2, plan.getDefense(1));
        assertEquals(d1, plan.getDefense(2));
        assertEquals(time2, d3.getHour());
        assertEquals(time2.plusMinutes(d3.getDuration() + Planning.breakDuration), d2.getHour());
        assertEquals(time2.plusMinutes(d3.getDuration() + d2.getDuration() + (Planning.breakDuration*2)), d1.getHour());
    }

    @Test
    void testDelayDefense() {
        assertEquals(0, plan.getNbDefense());
        plan.addDefense(d1); plan.addDefense(d2); plan.addDefense(d3);
        plan.scheduling(time2);
        assertEquals(d1, plan.getDefense(0));
        assertEquals(d2, plan.getDefense(1));
        assertEquals(d3, plan.getDefense(2));
        assertEquals(time2, d1.getHour());
        assertEquals(time2.plusMinutes(d1.getDuration() + Planning.breakDuration), d2.getHour());
        assertEquals(time2.plusMinutes(d1.getDuration() + d2.getDuration() + (Planning.breakDuration*2)), d3.getHour());
        // 5 minutes delay for d3 only
        plan.delayDefense(d3, 5);
        assertEquals(time2, d1.getHour());
        assertEquals(time2.plusMinutes(d1.getDuration() + Planning.breakDuration), d2.getHour());
        assertEquals(time2.plusMinutes(d1.getDuration() + d2.getDuration() + (Planning.breakDuration*2)+5), d3.getHour());
        // 10 minutes delay for d2 and d3
        plan.delayDefense(d2, 10);
        assertEquals(time2, d1.getHour());
        assertEquals(time2.plusMinutes(d1.getDuration() + Planning.breakDuration + 10), d2.getHour());
        assertEquals(time2.plusMinutes(d1.getDuration() + d2.getDuration() + (Planning.breakDuration*2) + 15), d3.getHour());
        // 20 minutes delay for every defenses
        plan.delayDefense(d1, 20);
        assertEquals(time2.plusMinutes(20), d1.getHour());
        assertEquals(time2.plusMinutes(d1.getDuration() + Planning.breakDuration + 30), d2.getHour());
        assertEquals(time2.plusMinutes(d1.getDuration() + d2.getDuration() + (Planning.breakDuration*2) + 35), d3.getHour());
    }
}
